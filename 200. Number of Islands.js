/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function(grid) {
    let visited = [];
    let  nextToExplore = [];
    
    const shifts = [
        {x: 0, y: -1},
        {x: 0, y: 1},
        {x: -1, y: 0},
        {x: 1, y: 0}
    ];
    
    function isVisited(x, y) {
        if (visited[x] && visited[x][y]) {
        	return true;
        }
    }

    function markVisited(x, y) {
        if (!visited[x]) {
        	visited[x] = [];
        }
        visited[x][y] = 1;
    }

    function isIsland(x, y) {
       	return !!(grid[x] && grid[x][y] && grid[x][y] != '0');
    }

    function queueNeighbours(x, y) {
		for (let i = 0; i < shifts.length; i++) {
    		nextToExplore.push({x: x + shifts[i].x, y: y + shifts[i].y});
		}
    }
    
    let islands = 0;
    for (var x = 0; x < grid.length; x++) {
        for (var y = 0; y < grid[x].length; y++) {
        	if (isIsland(x, y) && !isVisited(x, y)) {
        		queueNeighbours(x, y);
        		markVisited(x, y);
                islands++;
        	}
        	while (nextToExplore.length) {
        		let nextPoint = nextToExplore.pop();
        		if (nextPoint.x < 0 || nextPoint.y < 0) {
        			continue;
        		}
        		if (isIsland(nextPoint.x, nextPoint.y) && !isVisited(nextPoint.x, nextPoint.y)) {
        			queueNeighbours(nextPoint.x, nextPoint.y);
        		}
        		markVisited(nextPoint.x, nextPoint.y);
        	}
        }
    }
    return islands;
};

console.log(numIslands([["1","1","1","1","0"],["1","1","0","1","0"],["1","1","0","0","0"],["0","0","0","0","0"]]));
